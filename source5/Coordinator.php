<?php


namespace hw\second\source5;


class Coordinator extends Student
{
    public $name;
    public $surname;
    public $patronimic;

    public function setcourseCoordinatorName($name)
    {
        if ($this->isValidName($name)) {
            $this->courseCoordinatorName = $name;;
        }
    }
    private function isValidName($data): bool //Приватные методы внизу класса
    {
        return is_string($data) && strlen($data) >= 5;
    }

    public function setcourseCoordinatorSurName($surname)
    {
        if ($this->isValidSurName($surname)) {
            $this->courseCoordinatorName = $surname;;
        }
    }
    private function isValidSurName($data): bool //Приватные методы внизу класса
    {
        return is_string($data) && strlen($data) >= 5;
    }
    public function setcourseCoordinatorPatronimic($patronimic)
    {
        if ($this->isValidPatronimic($patronimic)) {
            $this->courseCoordinatorName = $patronimic;;
        }
    }
    private function isValidPatronimic($data): bool //Приватные методы внизу класса
    {
        return is_string($data) && strlen($data) >= 5;
    }
    public function getcourseCoordinatorName() {
        return $this->courseCoordinatorName;
    }
}

//Задача 20.1: Есть класс Student из задачи 8.
//Задача 20.2: Добавьте в этот класс валидатор для проверки имени и фамилии курс-координатора, сделайте так, чтобы его можно было использовать в классах наследниках.
//Задача 20.3: Создайте новый класс (на ваше усмотрение), который будет наследоваться от класса Student .
//Задача 20.4: Переопределите родительский метод валидатор, чтобы он проверял еще и отчество курс-координатора, помимо родительской проверки.