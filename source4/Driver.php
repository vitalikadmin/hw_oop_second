<?php


namespace hw\second\source4;


class Driver extends Employee
{
    public $driving_experience;
    public $driving_cacategory;

    public function setDrivingExperience($drivingEx) {
        $this->driving_experience = $drivingEx;
    }
    public function setDrivingCacategory($drivigCat) {
        $this->driving_cacategory = $drivigCat;
    }
    public function getDrivingExperience() {
        return $this->driving_experience;
    }
    public function getDrivingCacategory() {
        return $this->driving_cacategory;
    }
}
