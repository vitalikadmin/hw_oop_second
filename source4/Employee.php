<?php


namespace hw\second\source4;


class Employee extends Users
{
    public $surname;
    protected $work_day;

    public function setSurname($surname) {
        $this->surname = $surname;
    }
    public function setWorkDay($workday) {
        $this->work_day = $workday;
    }
    public function showWorkDay() {
        return $this->work_day;
    }
}
