<?php
namespace hw\second\source;

class Employee
{
    public $name;
    public $age;
    public $salary;

    public function getName() {
        return $this->name;
    }

    public function getAge() {
        return $this->age;
    }

    public function getSalary() {
        return $this->salary;
    }

    public function checkAge() {

        if ($this->age > 18) {
            return true;
        }
        return false;
    }
}