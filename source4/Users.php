<?php


namespace hw\second\source4;

//Задача 3.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).
//Задача 3.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.
//Задача 3.3: Создайте объект класса User с именем 'Коля' и возрастом 25. С помощью метода setAge поменяйте возраст на 30. Выведите новое значение возраста на экран.
//Задача 3.4: Модифицируйте метод setAge так, чтобы он вначале проверял, что переданный возраст больше или равен 18. Если это так - пусть метод меняет возраст пользователя, а если не так - то ничего не делает.

class Users
{
    public $name;
    public $age;
    protected $salary;

    public function setAge($age) {

        if ($this->age >= 18) {

            return $this->age = $age;

        }else {
            return $this->age = false;
        }
    }
    protected function setSalary($salary) {
        $this->salary = $salary;
    }
    public function showName() {
        return $this->name;
    }

}

