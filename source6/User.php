<?php

namespace hw\second\source6;

class User
{

    private $name;
    private $surname;
    private $birthday ;
    private $age;


    public function __construct($name, $surname, $birthday, $salary)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = date('Y-m-d', strtotime($birthday));
        $this->age = $this->calculateAge($this->birthday);
        $this->salary = $salary;
    }

    public function getAge() {
        return $this->age;
    }

    public function getName() {
        return $this->name;
    }
    public function getSurName() {
        return $this->surname;
    }
    public function getBirthDay() {
        return $this->birthday;
    }
    private function calculateAge($birthday) {
        $now = new \DateTime();
        $age = $now->diff(new \DateTime($birthday));
        return $age->y;
    }

}

