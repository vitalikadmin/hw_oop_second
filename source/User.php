<?php


namespace hw\second\source;

//Задача 14.1: Сделайте класс User, у которого будут приватные свойства surname (фамилия), name (имя) и patronymic (отчество).
//Задача 14.2: Эти свойства должны задаваться с помощью соответствующих сеттеров.
//Задача 14.3: Сделайте так, чтобы эти сеттеры вызывались цепочкой в любом порядке,
// а самым последним методом в цепочке можно было вызвать метод getFullName, который вернет ФИО юзера (первую букву фамилии, имени и отчества).
//Задача 14.4: Создайте объект и сразу же вызовите у него по цепочке все методы. Повторите в разной последовательности.

class User
{
    private $name;
    private $surname;
    private $patronymic;

    public function setName($name) {
        $this->name = $name;
    }
    public function setSurname($surname) {
        $this->surname = $surname;
    }
    public function setParronymic($patronymic) {
        $this->patronymic = $patronymic;
    }
    public function getFullName() {
       return substr($this->name[0], 0, 2) . '.' . substr($this->surname[0],0,2) . '.' . substr($this->patronymic[0],0,2);
    }
}

