<?php


namespace hw\second\source;

class Product
{
    private $name;
    private $price;
    private $quantity;

    public function __construct($name, $price, $quantity)
    {
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    public function showThis()
    {
        var_dump($this);
    }

    public function getProductName()
    {
        return $this->name;
    }

    public function getProductPrice()
    {
        return $this->price;
    }

    public function getProductQuantity()
    {
        return $this->quantity;
    }

    public function getCost()
    {
        $totalCost = $this->getProductPrice() * $this->getProductQuantity();
        return $totalCost;
    }

}