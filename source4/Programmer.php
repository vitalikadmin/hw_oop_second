<?php


namespace hw\second\source4;


class Programmer extends  Employee
{
    public $langs = [];
    protected $skil;

    public function setLengs($langs) {
        $this->langs[] = $langs;
    }
    public function getLengs() {
        foreach ($this->langs as $item) {
            echo ' '. $item;
        }

    }
    protected function setSkil($skil) {
        $this->skil = $skil;
    }
    public function showSkil() {
        return $this->skil;
    }

}
