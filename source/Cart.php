<?php


namespace hw\second\source;

class Cart
{
    private $products = [];

    public function add(Product $product)
    {
        $this->products[] = $product;
    }

    public function remove($name)
    {
        for ($i = 0; $i < count($this->products); $i++) {
            if ($this->products[$i]->getProductName() === $name) {
                array_splice($this->products, $i, 1);
            }

        }
    }

//находить суммарную стоимость продуктов.
    public function getTotalCost()
    {
        $result = 0;
        for ($i = 0; $i < count($this->products); $i++) {
            $result += $this->products[$i]->getProductPrice();
        }
        return $result;
    }

    // суммарное количество продуктов
    public function getTotalQuantity()
    {
        $result = 0;
        for ($i = 0; $i < count($this->products); $i++) {
            $result += $this->products[$i]->getProductQuantity();
        }
        return $result;
    }

    //среднюю стоимость продуктов
    public function getAvgPrice()
    {
        return $this->getTotalCost() / $this->getTotalQuantity();
    }


}

