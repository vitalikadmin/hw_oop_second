<?php
declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use hw\second\source\Cart as CartProduct;
use hw\second\source\Product as Product;
use hw\second\source1\Student as Students;
use hw\second\source1\Employee as Employees;
use hw\second\source2\User;

function pre()
{
    echo '<pre>' . PHP_EOL . '<pre/>';
}

echo 'TASK 12 ========================================================================================================>';
pre();

$city = new \hw\second\source\City();
$city->name = 'Percolst';
$city->population = 2000;
$city->fundation = 1983;


$props = [$city->name, $city->fundation, $city->population];

foreach ($props as $key) {
    echo $key . ' ';
}

pre();
echo 'TASK 13 ========================================================================================================>';
pre();


$employee = new \hw\second\source\Employee();
$employee->name = 'Vasiliy';
$employee->age = 35;
echo $employee->getAge();
pre();

$methods = ['method1' => $employee->getName(), 'method2' => $employee->getAge()];

foreach ($methods as $key) {
    echo $key . ' ';
}

pre();
echo 'TASK 14 ========================================================================================================>';
pre();

$user = new \hw\second\source\User();

echo $user->setName('Vitaliy').$user->setSurname('Sonko').$user->setParronymic('Vasilievich').$user->getFullName();


pre();
echo 'TASK 15 ========================================================================================================>';
pre();

$product1 = new Product('iphone', 5000, 5);
$product2 = new Product('samsung', 5400, 10);
$product3 = new Product('dell', 25000, 5);
pre();
echo $product1->getProductName() . ' ' . $product1->getProductPrice() . ' ' . $product1->getProductQuantity();
pre();
$cart = new CartProduct();

$cart->add($product1);
$cart->add($product2);
$cart->add($product3);

$cart->remove('iphone');

echo $cart->getTotalCost();
pre();
echo $cart->getTotalQuantity();
pre();
echo $cart->getAvgPrice();

pre();
echo 'TASK 16 ========================================================================================================>';
pre();

$employeer1 = new Employees('Stepan', 2000);
$employeer2 = new Employees('Mikola', 60000);
$employeer3 = new Employees('Yuriy', 500);


$student1 = new Students('Sasha', 5);
$student2 = new Students('Andrey', 40);
$student3 = new Students('Nikita', 500);

$arr = [$employeer1, $student1, $employeer2, $student2, $employeer3, $student3];

$salarys = 0;
$scholarships = 0;

for ($i = 0; $i < count($arr); $i++) {
    echo $arr[$i]->name;
    pre();
}
for ($i = 0; $i < count($arr); $i++) {
    if (property_exists($arr[$i], 'salary')) {
        $salarys += $arr[$i]->salary;
    } elseif (property_exists($arr[$i], 'scholarship')) {
        $scholarships += $arr[$i]->scholarship;
    }
}
echo $salarys;
pre();
echo $scholarships;

pre();
echo 'TASK 17 ========================================================================================================>';
pre();

$user0 = new \hw\second\source2\User('Fedor', 'Prostokvasha');
$user1 = new \hw\second\source2\User('Petp', 'Poroshenko');
$user2 = new \hw\second\source2\User('Leonid', 'Kuchma');

$employeer0 = new \hw\second\source2\Employee(1000);
$employeer1 = new \hw\second\source2\Employee(1500);
$employeer2 = new \hw\second\source2\Employee(2000);

$City0 = new \hw\second\source2\City('Kiev', 2000);
$City1 = new \hw\second\source2\City('Brovary', 1000);
$City2 = new \hw\second\source2\City('Vishneve', 5000);

$arr = [$user0, $user1, $user2, $employeer0, $employeer1, $employeer2, $City0, $City1, $City2 ];

//is_a — Проверяет, принадлежит ли объект к данному классу или является ли этот класс одним из его родителей

    for ($i = 0; $i < count($arr); $i++) {
        if (is_a($arr[$i], User::class)) {

            echo $arr[$i]->name . ' ';

        }
    }
        echo 'is an';
        pre();

for ($i = 0; $i < count($arr); $i++) {
    if ((!is_a($arr[$i], User::class))){
        echo $arr[$i]->name . ' ' ;
    }
}
echo 'is not';
pre();

//Оператор instanceof используется для определения того, является ли текущий объект экземпляром указанного класса

for ($i = 0;$i < count($arr); $i++) {
    if ($arr[$i] instanceof User) {
        echo $arr[$i]->name . ' ';
    }
}
echo 'Принадлежат только User::class';
pre();

pre();
echo 'TASK 18 ========================================================================================================>';
pre();

$programmer = new \hw\second\source3\Post('programmer', 10000);
$manager = new \hw\second\source3\Post('manager', 2000);
$driver = new \hw\second\source3\Post('driver', 1000);

$employee = new \hw\second\source3\Employee('Yuriy', 'Vichorski', $programmer);

$employee->changePost($manager);

echo $employee->getName().' '.$employee->getSurname() .' '. $programmer->getName() .' '. $programmer->getSalary();

pre();
echo 'TASK 19 ========================================================================================================>';
pre();

use \hw\second\source4\Users as Users;

$user5 = new Users();


$user5->name = 'Devid';
echo $user5->showName();

$prog = new \hw\second\source4\Programmer();
$emp = new \hw\second\source4\Employee();


$a = 'PHP';
$b = 'JS';
$c = 'C++';
$d = 'C#';

$prog->setLengs($a);
$prog->setLengs($b);
$prog->setLengs($c);
$prog->setLengs($d);

$prog->getLengs();
pre();

$emp->setWorkDay('sundey');
echo $emp->showWorkDay();
pre();

pre();
echo 'TASK 20 ========================================================================================================>';
pre();

$curscoordin = new \hw\second\source5\Coordinator();

$curscoordin->setcourseCoordinatorName('Yudjin');

echo $curscoordin->getcourseCoordinatorName();

pre();
echo 'TASK 21 ========================================================================================================>';
pre();



$birthUser = new \hw\second\source6\User('Tolya', 'Kaplyuk', '1983-05-04', 500);

echo $birthUser->getName();

echo ' Сегодня '.$birthUser->getAge() . ' Лет';




pre();
echo 'TASK 22 ========================================================================================================>';
pre();

//Задача 22.1: Сделайте класс ArraySumHelper.
//Задача 22.2: Добавьте в него метод, который будет принимать первым аргументом массив чисел, а вторым - число,
//во сколько раз нужно умножить каждый из элементов массива. Метод должен возвращать итоговый массив с умноженными элементами.
//Задача 22.3: Пусть дан массив с числами. Найдите с помощью класса  ArraySumHelper сумму квадратов элементов этого массива.
//Задача 22.4: Переделайте класс ArraySumHelper на работу сo статикой.