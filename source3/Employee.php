<?php


namespace hw\second\source3;


class Employee
{
    public $name;
    public $surname;

    public function __construct($name, $surname, Post $post)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $post;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function setSurname($surname) {
        $this->surname = $surname;
    }
    public function getName() {
        return $this->name;
    }
    public function getSurname() {
        return $this->surname;
    }
    public function changePost(Post $post) {
        $this->post = $post;
    }
}
